# What's New

**CAMP Heater control  : control software for CH-B, CH-N heaters for in-situ heating**



# CH controller_V0.2.2 (2020-09-19)

**New features**

1. add control to select Channel and node number for in the debug pannel to be applicable for one channel applications  

**Bug Fix**

1. Compatibility for one channel sourcemeters 


# CH controller_V0.2.1 (2018-10-02)

**New features**

1. add separate ChA and ChB version for partially damaged heaters 

**Updates**

1. Add version number to bottom of the SW of both panels
2. Update alpha value
3. Tidy up the black diagram

**Bug Fix**

1. PID control bug fixed


